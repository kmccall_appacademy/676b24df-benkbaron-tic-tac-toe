class Board

  def initialize (grid=nil)
    grid ? @grid = grid : @grid = Array.new(3) {Array.new(3)}
  end

  attr_reader :grid

  def place_mark (pos, mark)
    self[pos] = mark
  end

  def empty?(pos=nil)
    pos ? self[pos] == nil : grid.empty?
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, mark)
    x, y = pos
    grid[x][y] = mark
  end

  def winner
    who_won
  end

  def over?
    return true if self.winner
    grid.each { |row| return false if row.include?(nil) }

    true
  end

  private

  def who_won
    adjacent_threes = []

    grid.each {|row| adjacent_threes << row}

    (0..2).each do |row|
      temp = []
      (0..2).each do |col|
        temp << grid[col][row]
      end
      adjacent_threes << temp
    end

    adjacent_threes << [grid[0][0], grid[1][1], grid[2][2]]
    adjacent_threes << [grid[0][2], grid[1][1], grid[2][0]]

    adjacent_threes.each do |three|
      return :X if three.all? {|el| el == :X}
      return :O if three.all? {|el| el == :O}
    end

    nil
  end

end
