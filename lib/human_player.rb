class HumanPlayer

  def initialize (name)
    @name = name
  end

  attr_reader :name, :board

  attr_accessor :mark

  def get_move
    puts "Where do you want to move? Ex: 1, 2"
    pos = gets.chomp.split(", ").map! {|num| num.to_i}
    return pos if board == nil
    return pos if valid_move?(pos)
    puts "Sorry, that's not an empty space."
    get_move
  end

  def display(board)
    @board = board
    sleep(1)
    puts
    board.grid.each do |row|
      p row
    end
    puts
  end

  private

  def valid_move?(pos)
    board.empty?(pos)
  end

end
