require 'byebug'

class ComputerPlayer

  def initialize (name)
    @name = name
  end

  attr_reader :name, :board

  attr_accessor :mark

  def display(board)
    puts
    board.grid.each {|row| p row }
    puts
    @board = board
  end

  def get_move
    computer_move_coords
  end

  private

  def computer_move_coords
    move_options.each do |pos|
      board.place_mark(pos, mark)
      if board.winner
        board.place_mark(pos, nil)
        return pos
      else
        board.place_mark(pos, nil)
      end
    end

    move_options.sample
  end

  def move_options
    empty_coords = []

    (0..2).each do |row|
      (0..2).each do |col|
        empty_coords << [row, col] if board.grid[row][col].nil?
      end
    end
    empty_coords
  end
  
end
