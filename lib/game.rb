require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
class Game

  def initialize(player_one, player_two, grid=nil)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new(grid)
    @current_player = player_one
    @player_one.mark = :X
    @player_two.mark = :O
  end

  attr_reader :board, :player_one, :player_two, :current_player, :mark

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def switch_players!
    if current_player == player_one
      @current_player = player_two
    else
      @current_player = player_one
    end
  end

  def play
    current_player.display(board)
    until board.over?
      play_turn
    end

    if board.winner
      switch_players!
      puts "Congratulations, #{current_player.name}! You won!"
    else
      puts "Yay, no one lost! You tied."
    end
  end

end

if $PROGRAM_NAME == __FILE__
  compy = ComputerPlayer.new("Compy")
  ben = HumanPlayer.new("Ben")

  game = Game.new(compy, ben)

  game.play
end
